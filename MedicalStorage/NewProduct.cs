﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MedicalStorage
{
    public partial class NewProduct : Form
    {
        List<Product> list;
        public NewProduct(List<Product> list1)
        {
            InitializeComponent();
            list = list1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Product product = new Product
            {
                Name = textBox1.Text,
                Description = textBox2.Text,
                _Category = (Category)comboBox1.SelectedIndex
            };
            if (product.WriteDataInDB(string.Format("insert into Product(Product,Description,Category) values('{0}','{1}',{2})", product.Name, product.Description, (int)product._Category)))
            {
                list.Add(product);
                MessageBox.Show("Данные записаны успешно!", "Успех!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Ошибка при записи:(", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
