﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalStorage
{
     interface IProps
    {
        string Name { get; set; }
        string Address { get; set; }
        string Phone { get; set; }
        List<object> Show();
        void Add();
    }
}
