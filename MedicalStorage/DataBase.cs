﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalStorage
{
    public abstract class DataBase
    {
        public DataTable ReadFromDB(string table)
        {
            SqlConnection connect = new SqlConnection(SettingsFile.Default.ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter(string.Format("select * from {0}", table), connect);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);

            return dataTable;
        }

        public bool WriteDataInDB(string commandText)
        {
            try 
            {
                SqlConnection connect = new SqlConnection(SettingsFile.Default.ConnectionString);
                SqlCommand command = new SqlCommand(string.Format("{0}", commandText), connect);
                connect.Open();
                command.ExecuteNonQuery();
                connect.Close();

                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
