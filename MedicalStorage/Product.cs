﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalStorage
{
    public enum Category
    {
        tablet,
        liquid,
        etc
    }
    public class Product : DataBase
    {
        public string Name { get; set; }
        public Category _Category { get; set; }
        public string Description { get; set; }
    }
}
