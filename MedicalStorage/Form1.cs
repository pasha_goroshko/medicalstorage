﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MedicalStorage
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AuthWindow auth = new AuthWindow();
            auth.ShowDialog();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            ProductForm productForm = new ProductForm();
            productForm.ShowDialog();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            FormStocks stocks = new FormStocks();
            stocks.ShowDialog();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FormStocks formStock = new FormStocks();
            formStock.ShowDialog();
        }
    }
}
