﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MedicalStorage
{
    public partial class ProductForm : Form
    {
        List<Product> list = new List<Product>();
        public ProductForm()
        {
            InitializeComponent();
            DataTable dt = new Product().ReadFromDB("Product");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(
                    new Product
                {
                    Name = dt.Rows[i]["Product"].ToString(),
                    Description = dt.Rows[i]["Description"].ToString(),
                    _Category = (Category)int.Parse(dt.Rows[i]["Category"].ToString())
                });
            }
            RefreshData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            NewProduct addProduct = new NewProduct(list);
            addProduct.ShowDialog();
            RefreshData();
        }

        #region НЕ ОТКРЫВАТЬ!!!
        void RefreshData()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = list;
            dataGridView1.Columns[0].HeaderText = "Наименование товара";
            dataGridView1.Columns[1].HeaderText = "Категория";
            dataGridView1.Columns[2].HeaderText = "Описание";
        }
        #endregion
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            string command = String.Format("Delete From Product where Product='{0}'", dataGridView1.Rows.SelectedCells[0].Value);
           if( new Product().WriteDataInDB(command))
           {
               
           }
            RefreshData();
        }
    }
}
